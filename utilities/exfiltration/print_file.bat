@ECHO OFF
:: string terminator: chose something that won't show up in the input file
SET strterm=!
:: read first line of input file
SET /P mytext=<%1
:: add string terminator to input
SET tmp=%mytext%%strterm%
:loop
:: get first character from input
SET char=%tmp:~0,1%
:: remove first character from input
SET tmp=%tmp:~1%
:: do something with %char%, e.g. simply print it out
< NUL SET /P =%char%
:: repeat until only the string terminator is left
IF NOT "%tmp%" == "%strterm%" GOTO loop